﻿using CI.Dto.Dtos;

namespace CI.Service.Interface.IService
{
    public interface ICustomerService
    {
        CustomerDto Get(string customerId, string customerEmail);
    }
}
