﻿using CI.Dto.Dtos;

namespace CI.DataAccess.Interface.IRepositories
{
    public interface ICustomerRepository
    {
        CustomerDto GetById(int customerId);

        CustomerDto GetByEmail(string customerEmail);

        CustomerDto GetByEmailAndId(string customerEmail, int customerId);
    }
}
