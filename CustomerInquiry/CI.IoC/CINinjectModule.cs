﻿using CI.DataAccess.Interface.IRepositories;
using CI.DataAccess.Repositories;
using CI.Service.Interface.IService;
using CI.Service.Services;
using Ninject.Modules;

namespace CI.IoC
{
    public sealed class CINinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(ICustomerRepository)).To(typeof(CustomerRepository));
            Bind(typeof(ICustomerService)).To(typeof(CustomerService));
        }
    }
}
