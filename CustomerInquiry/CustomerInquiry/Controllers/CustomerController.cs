﻿using CI.Service.Interface.IService;
using System;
using System.Web.Http;

namespace CustomerInquiry.Controllers
{
    public class CustomerController : ApiController
    {
        private readonly ICustomerService _customerService;

        public CustomerController() { }

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public IHttpActionResult Get(string customerID = null, string email = null)
        {
            try
            {
                var customer = _customerService.Get(customerID, email);
                if (customer == null)
                {
                    return NotFound();
                }

                return Ok(customer);
            }
            catch (ArgumentException exception)
            {
                return BadRequest(exception.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }
    }
}
