﻿using CI.DataAccess.Interface.IRepositories;
using CI.Dto.Dtos;
using CI.Service.Interface.IService;
using System;
using System.Text.RegularExpressions;

namespace CI.Service.Services
{
    public sealed class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        private const string _className = nameof(CustomerService);

        public CustomerService(ICustomerRepository customerRepository)
        {
            Logger.InitLogger();
            _customerRepository = customerRepository;
        }

        private CustomerDto GetByEmail(string customerEmail)
        {
            return _customerRepository.GetByEmail(customerEmail);
        }

        private CustomerDto GetByEmailAndId(string customerEmail, int customerId)
        {
            return _customerRepository.GetByEmailAndId(customerEmail, customerId);
        }

        private CustomerDto GetById(int customerId)
        {
            return _customerRepository.GetById(customerId);
        }

        private void Validate(string customerId, string customerEmail)
        {
            if (!string.IsNullOrEmpty(customerId) && (!Regex.IsMatch(customerId,
                @"^(?:[0-9]{1,10})$")))
            {
                throw new ArgumentException("Invalid Customer ID");
            }
            if (!string.IsNullOrEmpty(customerEmail) && (!Regex.IsMatch(customerEmail,
                @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")
                || customerEmail.Length > 25))
            {
                throw new ArgumentException("Invalid Email");
            }
        }

        public CustomerDto Get(string customerId, string customerEmail)
        {
            Validate(customerId, customerEmail);
            try
            {
                if (customerId != null)
                {
                    var id = Convert.ToInt32(customerId);
                    if (string.IsNullOrEmpty(customerEmail))
                    {
                        return GetById(id);
                    }
                    else
                    {
                        return GetByEmailAndId(customerEmail, id);
                    }
                }
                else if (!string.IsNullOrEmpty(customerEmail))
                {
                    return GetByEmail(customerEmail);
                }
                else
                {
                    throw new ArgumentException("No inquiry criteria");
                }
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"{_className}: {ex.Message}");
                return null;
            }

        }
    }
}
