﻿using log4net;
using log4net.Config;

namespace CI.Service
{
    internal static class Logger
    {
        public static ILog Log { get; } = LogManager.GetLogger("BLLogger");

        public static void InitLogger()
        {
            XmlConfigurator.Configure();
        }
    }
}
