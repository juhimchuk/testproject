﻿namespace CI.Dto.Enums
{
    public enum Currency
    {
        USD,
        JPY,
        THB,
        SGD
    }
}
