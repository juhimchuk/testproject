﻿namespace CI.Dto.Enums
{
    public enum Status
    {
        Success,
        Failed,
        Canceled
    }
}
