﻿using CI.Dto.Enums;
using System;

namespace CI.Dto.Dtos
{
    public sealed class TransactionDto
    {
        public int Id { get; set; }
        
        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        public Currency CurrencyCode { get; set; }

        public Status Status { get; set; }
    }
}
