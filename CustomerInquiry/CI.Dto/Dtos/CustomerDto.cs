﻿using System.Collections.Generic;

namespace CI.Dto.Dtos
{
    public class CustomerDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public int MobileNo { get; set; }

        public virtual IEnumerable<TransactionDto> Transactions { get; set; }
    }
}
