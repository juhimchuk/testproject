﻿using CI.DataAccess.Entities;
using CI.DataAccess.Interface.IRepositories;
using CI.Dto.Dtos;
using System.Linq;

namespace CI.DataAccess.Repositories
{
    public sealed class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository() : base(new CIContext())
        {
        }

        public CustomerDto GetByEmail(string customerEmail)
        {
            var entity = base.Get(e => e.Email.Equals(customerEmail)).SingleOrDefault();
            return EntityToDto(entity);
        }

        public CustomerDto GetByEmailAndId(string customerEmail, int customerId)
        {
            var entity = base.Get(e => e.Email.Equals(customerEmail) && e.Id == customerId).SingleOrDefault();
            return EntityToDto(entity);
        }

        public CustomerDto GetById(int customerId)
        {
            var entity = base.Get(e => e.Id == customerId).SingleOrDefault();
            return EntityToDto(entity);
        }

        private CustomerDto EntityToDto(Customer model)
        {
            if (model == null)
            {
                return null;
            }

            return new CustomerDto
            {
                Id = model.Id,
                Name = model.Name,
                Email = model.Email,
                MobileNo = model.MobileNo,
                Transactions = model.Transactions?.OrderByDescending(t=>t.Date).Take(5).Select(EntityToDto)
            };
        }

        private TransactionDto EntityToDto(Transaction model)
        {
            return new TransactionDto
            {
                Id = model.Id,
                Amount = model.Amount,
                Date = model.Date,
                CurrencyCode = model.CurrencyCode,
                Status = model.Status
            };
        }
    }
}
