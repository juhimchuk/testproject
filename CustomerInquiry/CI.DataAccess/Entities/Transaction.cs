﻿using CI.Dto.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CI.DataAccess.Entities
{
    public class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public DateTime Date { get; set; }

        [RegularExpression(@"^\d+\.\d{0,2}$")]
        public decimal Amount { get; set; }

        public Currency CurrencyCode { get; set; }

        public Status Status { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
    }
}
