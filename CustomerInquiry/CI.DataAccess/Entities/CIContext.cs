﻿using System.Data.Entity;

namespace CI.DataAccess.Entities
{
    public partial class CIContext : DbContext
    {
        public CIContext() : base("name=CIContext")
        {

        }

        public virtual DbSet<Customer> Customers { get; set; }

        public virtual DbSet<Transaction> Transactions { get; set; }
    }
}
