﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CI.DataAccess.Entities
{
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Range(0, int.MaxValue)]
        public int Id { get; set; }

        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [MaxLength(25)]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")]
        public string Email { get; set; }

        [Range(0, int.MaxValue)]
        public int MobileNo { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
